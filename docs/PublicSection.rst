.. aimms:module:: axll



PublicSection
*********************

.. toctree::

   Workbook_Management
   Utilities
   Scalar_Read_Write
   Sets_Read_Write
   Multi_Dimensional_Data
   
.. toctree::

   
.. aimms:parameter:: CalendarElementsAsStrings

    :attribute Default: 0

    :attribute Property: NoSave


    Allowed values: 0 (=default) or 1.
    
    By default, when writing elements of a calendar set to a sheet, the written cells will be formatted as a Date (which always includes at least a year, a month and a day).
    If the format of the calendar does not included all these parts, it might be more convenient to write the elements as simple strings according to the calendar format.
    For example a calendar with elements { 2016, 2017, 2018 } will then be written as "2016", "2017", "2018" instead of 2016/1/1, 2017/1/1, 2018/1/1
    
    Similarly when reading calendar elements, by default the library expects cells formatted as Date, but when this option is set to 1 it expects strings according to the 
    date format of the calendar.

.. aimms:parameter:: WriteInfValueAsString

    :attribute Default: 0

    :attribute Property: NoSave


    Allowed values: 0 (=default) or 1.
    
    By default, when writing numerical data that contains the value `INF` or `-INF`, 
    these values are written to a cell as the number 1E+150 and -1E+150 respectively.
    If you set this option to 1, these values will be written not a as numbers but as strings ("INF" and "-INF").
    This might be convenient to visually inspect the values in Excel, but please be aware that Excel formulas that 
    operates on a range with both numerical and string values present, might not work as expected.

.. aimms:parameter:: KeepExistingCellFormats

    :attribute Default: 0

    :attribute Property: NoSave


    Allowed values: 0 (=default) or 1.
    
    By default, when writing data into a cell, AIMMS checks whether the specified format of that cell matches the value that is written.
    If it does not match (for example if a string value is written into a cell that is formatted as Number) then it changes the format 
    of the cell such that the value can be correctly written.
    If you set this option to 1, the format will *not* be checked and values are just copied to the cell, leaving the format as is.
    
    Setting this option to 1 is especially usefull when your sheet contains cells with a custom format for which it unclear what 
    type of values can be written into it.

.. aimms:parameter:: TrimLeadingAndTrailingSpaces

    :attribute Default: 0

    :attribute Property: NoSave


    Allowed values: 0 (=default) or 1.
    
    By default, when reading string valued cells, any leading or trailing spaces in a cell are interpreted by AIMMS as part of string (or element name).
    If you set this option to 1 prior to reading any data these leading and/pr trailing spaces will be removed.
    In other words: a cell with value "  my cell value " will be passed to AIMMS as "my cell value".
    
    This option does not have an effect on strings or elements that are written to the spreadsheet.
